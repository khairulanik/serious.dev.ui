export function flattenCollection(collection, array = false) {
  if (array) return collection.map((item) => flattenCollection({...item.attributes, id: item.id}))
  const result = {}
  Object.keys(collection).forEach((key) => {
    if (Array.isArray(collection[key]?.data)) {
      result[key] = flattenCollection(collection[key].data, true)
    } else {
      result[key] = collection[key]
    }
  })
  return result
}
