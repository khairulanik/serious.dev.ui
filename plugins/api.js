import createRepository from '~/services';

export default (ctx, inject) => {
  ctx.$axios.setHeader(
    'Authorization',
    `bearer ${process.env.API_TOKEN}`
  )
  const repositoryWithAxios = createRepository(ctx.$axios);

  const repositories = {
    articles: repositoryWithAxios('articles'),
    // users: repositoryWithAxios('users'),
    // articles: repositoryWithAxios('articles')
  }

  inject('api', repositories);
}
