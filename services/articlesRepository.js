const resource = '/articles'
const articlesApi = ($client) => ({
  get(query = '') {
    return $client.get(`${resource}?populate=*&&${query}`)
    .then((res) => ({data: res.data.data, meta: res.data.meta}))
  },

  getBySlug(slug) {
    return $client
      .get(
        `${resource}?filters[slug]=${slug}&populate[0]=seo&populate[1]=seo.metaImage&populate=image`
      )
      .then((res) => res.data.data)
  },

  post(payload) {
    return $client.post(`${resource}`, payload)
  },

  find(id) {
    return $client.get(`${resource}/${id}`)
  },

  update(id, payload) {
    return $client.put(`${resource}/${id}`, payload)
  },

  delete(id) {
    return $client.delete(`${resource}/${id}`)
  },
})

export default articlesApi
