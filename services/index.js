import articlesApi from "./articlesRepository";

const apiMap = {
    articles: articlesApi
}

const createRepository = ($client) => (api) => apiMap[api]($client)

export default createRepository;