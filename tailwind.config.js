module.exports = {
  darkMode: 'class',
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  theme: {
    fontFamily: {
      'cursive': ['Pacifico', 'cursive'],
      'sans': ['Noto Sans', 'sans-serif']
    },
    extend: {
      colors: {
        dark: '#34495E',
        light: '#ffffff',
        accent: '#41B883'
      },
    },
  },
  plugins: [],
}
